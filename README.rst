=======
Downdoc
=======


.. image:: https://img.shields.io/pypi/v/downdoc.svg
        :target: https://pypi.python.org/pypi/downdoc

.. image:: https://img.shields.io/travis/src-r-r/downdoc.svg
        :target: https://travis-ci.com/src-r-r/downdoc

.. image:: https://readthedocs.org/projects/downdoc/badge/?version=latest
        :target: https://downdoc.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Download Python documentation (from RTD) automatically.


* Free software: MIT license
* Documentation: https://downdoc.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
