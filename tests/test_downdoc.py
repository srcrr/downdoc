#!/usr/bin/env python

"""Tests for `downdoc` package."""

import os
import shutil
import unittest
import string
import logging
import logging.config
import random
from click.testing import CliRunner

from yaml import load, dump

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from downdoc import utils

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

HERE = os.path.abspath(os.path.dirname(__file__))


def random_string(sz=100):
    return "".join([random.choice(string.printable) for i in range(0, 100)])


class TestUtils(unittest.TestCase):
    def setUp(self):
        self.existing_files = {"text": ["existing1.txt",], "yaml": ["existing1.yaml",]}
        self.absent_files = {"text": ["abset1.txt",], "yaml": ["absent1.yaml",]}
        for e in self.existing_files["yaml"]:
            with open(e, "w+") as yaml:
                dump(
                    {"a": random_string(10), "b": random_string(10)},
                    yaml,
                    Dumper=Dumper,
                )
        for e in self.existing_files["text"]:
            with open(e, "w+") as f:
                f.write(random_string(random.randint(100, 1000)))

    def tearDown(self):
        for k in self.existing_files.keys():
            for e in self.existing_files[k]:
                os.remove(e)

    def test_safe_open_success(self):
        f = utils._safe_open(self.existing_files["text"][0], "r", True)
        self.assertIsNotNone(f)
        f.close()

    def test_safe_open_failure(self):
        with self.assertRaises(Exception):
            f = utils._safe_open(self.absent_files["text"][0], "r", False)

    def test_yaml_read_first_success(self):
        data = utils.yaml_read_first(
            self.absent_files["yaml"][0], self.existing_files["yaml"][0]
        )
        self.assertIsNotNone(data)
        self.assertIn("a", data)

    def test_yaml_read_first_failure(self):
        data = utils.yaml_read_first(
            self.absent_files["yaml"][0], self.absent_files["yaml"][0]
        )
        self.assertIsNone(data)


def try_delete(path):
    if os.path.exists(path):
        shutil.rmtree(path)


class TestSquash(unittest.TestCase):
    def setUp(self):
        try_delete(os.path.join(HERE, "squash"))
        try_delete(os.path.join(HERE, "successful_squash"))
        try_delete(os.path.join(HERE, "successful_squash__tmp"))
        self.squash_target = os.path.join(HERE, "squash", "path", "some", "dir")
        os.makedirs(self.squash_target)
        for i in range(0, 5):
            with open(os.path.join(self.squash_target, f"{i}.txt"), "w+") as f:
                f.write(random_string(100))

    def tearDown(self):
        # Squash cleanup
        try_delete(os.path.join(HERE, "squash"))
        try_delete(os.path.join(HERE, "successful_squash"))
        try_delete(os.path.join(HERE, "successful_squash__tmp"))

    def test_squash_path(self):
        src = os.path.join(HERE, "squash")
        dest = os.path.join(HERE, "successful_squash")
        utils.squash_path(src, dest)
        self.assertEqual(len(os.listdir(dest)), 5)
