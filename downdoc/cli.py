"""Console script for downdoc."""
import sys
import click
import os
import logging.config
import logging
from dpath.util import get as dget, set as dset, merge as dmerge
from dpath.util import get as dget, set as dset, merge as dmerge

from downdoc.utils import yaml_read_first
from downdoc.downdoc import download_package, update_downdoc_index

USER_CONFIG_PATH = os.path.join(
    os.path.expanduser("~"), ".local", "config", "downdoc.yaml"
)

FALLBACK_CONFIG_PATH = os.path.join(
    os.path.abspath(os.path.dirname(__file__)), "..", "downdoc-config.yaml"
)

FORMAT_CHOICES = ["epub", "html", "htmlzip", "pdf"]

DEFAULT_DEBUG = False

log = logging.getLogger(__name__)


@click.group()
@click.option("--config", "-c", default=USER_CONFIG_PATH)
@click.option("--debug", "-d", is_flag=True)
def main(config=USER_CONFIG_PATH, debug=DEFAULT_DEBUG):
    pass


@click.command()
@click.argument("package_name")
def versions(package_name, args=None, config=USER_CONFIG_PATH, debug=DEFAULT_DEBUG):
    downdoc_config = yaml_read_first(config, FALLBACK_CONFIG_PATH, fail_silently=False)
    logging.config.dictConfig(dget(downdoc_config, "logging"))
    # download_package(package_name, downdoc_config)
    pass


@click.command()
@click.argument("package_name")
def formats(package_name, args=None, config=USER_CONFIG_PATH, debug=DEFAULT_DEBUG):
    downdoc_config = yaml_read_first(config, FALLBACK_CONFIG_PATH, fail_silently=False)
    logging.config.dictConfig(dget(downdoc_config, "logging"))
    # download_package(package_name, downdoc_config)
    pass

@click.command()
def reindex(config=USER_CONFIG_PATH, debug=DEFAULT_DEBUG):
    downdoc_config = yaml_read_first(config, FALLBACK_CONFIG_PATH, fail_silently=True)
    logging.config.dictConfig(dget(downdoc_config, "logging"))
    update_downdoc_index(downdoc_config)
    pass


@click.command()
@click.option("--format", "-f", default=None)
@click.option("--version", "-v", default="latest")
@click.argument("package_name")
def download(
    package_name,
    args=None,
    config=USER_CONFIG_PATH,
    debug=DEFAULT_DEBUG,
    format=None,
    version="latest",
):
    cfg = yaml_read_first(config, FALLBACK_CONFIG_PATH)
    assert cfg is not None
    logging.config.dictConfig(dget(cfg, "logging"))
    log.debug("config set")
    if format:
        dset(cfg, "download/format", format)
    download_package(package_name, cfg, version=version)


main.add_command(versions)
main.add_command(download)
main.add_command(reindex)

if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
