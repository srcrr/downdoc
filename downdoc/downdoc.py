"""Main module."""

import os
import sys
from dpath.util import get as dget, set as dset, merge as dmerge
import requests
import zipfile
import shutil
import logging
from jinja2 import Template
from bs4 import BeautifulSoup

from downdoc.utils import squash_path, unslugify

BS4_PARSER = "html.parser"

log = logging.getLogger(__name__)

HERE = os.path.abspath(os.path.dirname(__file__))
PROJ_ROOT = os.path.abspath(os.path.join(HERE, ".."))

DEFAULT_STATIC_DIR = os.path.join(PROJ_ROOT, "templates/_static")


def get_target_path(cfg):
    target_path = dget(cfg, "download/target")
    return os.path.join(*target_path).format(home=os.path.expanduser("~"))


def ensure_target_exists(cfg):
    p = get_target_path(cfg)
    if not os.path.exists(p):
        os.makedirs(p)


def ensure_downloads_dir(cfg):
    p = get_target_path(cfg)
    dl = os.path.join(p, "_downloads")
    if not os.path.exists(dl):
        os.makedirs(dl)


def request_download(url, download_path):
    resp = requests.get(url)
    if resp.status_code >= 300:
        log.error("While downloading %s", url)
        log.error(resp)
        return 1
    with open(download_path, "wb+") as download_file:
        log.debug("Writing %d bytes to %s", len(resp.content), download_path)
        download_file.write(resp.content)


def download_package(package_name, cfg, **kwargs):
    ensure_target_exists(cfg)
    ensure_downloads_dir(cfg)
    url_base = dget(cfg, "url")
    dl_format = dget(cfg, "download/format") or "htmlzip"
    extractzip = dget(cfg, "download/extractzip") or True
    keep_downloads = dget(cfg, "download/keep_downloads") or True
    version = kwargs.get("version", "latest")
    url = url_base.format(package_name=package_name, format=dl_format, version=version)
    target_path = get_target_path(cfg)
    extracted_path = os.path.join(target_path, package_name)
    ext = dl_format
    if dl_format == "htmlzip":
        ext = "zip"
    download_path = os.path.join(target_path, "_downloads", package_name + "." + ext)
    if not os.path.exists(download_path):
        if request_download(url, download_path) == 1:
            log.error("Could not download documentation for %s", package_name)
            log.error("Package: %s", package_name)
            log.error("Version: %s", version)
            log.error("Format:  %s", dl_format)
            log.error("Perhaps try a different format or a different version.")
            sys.exit(1)
    else:
        log.info("%s alredy exists.", download_path)

    if extractzip and download_path.endswith(".zip"):
        with zipfile.ZipFile(download_path) as zf:
            log.debug("Extracting %s to %s", download_path, extracted_path)
            zf.extractall(path=extracted_path)

    if not download_path.endswith(".zip"):
        shutil.copy(download_path, target_path)

    squash_path(extracted_path, extracted_path)

    if not keep_downloads:
        os.path.remove(download_path)

    update_downdoc_index(cfg)
    index_url = f"file://{target_path}/index.html"
    log.info("The latest DownDoc page can be viewed at %s", index_url)


def get_soup(path):
    with open(path, "r") as f:
        return BeautifulSoup(f.read(), BS4_PARSER)


def _index_path(path):
    return os.path.join(path, "index.html")


def contains_index(path):
    return os.path.exists(_index_path(path))


def get_index_path(path):
    if contains_index(path):
        return _index_path(path)
    return None


def find_title(soup):
    title = soup.find("title")
    if title:
        return title.text
    return None


def find_img_with_fuzzy_class(soup, kw, attr="src"):
    for img in soup.find_all("img"):
        for _class in img.attrs.get("class", []):
            if not _class:
                continue
            # log.debug("%s in %s? %s", kw, _class.lower(), kw in _class.lower())
            if kw in _class.lower():
                return img.attrs.get(attr)
    return None


def find_logo(soup):
    return find_img_with_fuzzy_class(soup, "logo")


def find_banner(soup):
    return find_img_with_fuzzy_class(soup, "banner")


def find_favicon(soup):
    favicon = soup.find("link", attrs={"rel": "shortcut icon"})
    if favicon:
        return favicon.attrs.get("href")
    return None


def collect_doc_links(cfg):
    target_path = get_target_path(cfg)
    doc_links = []
    for name in os.listdir(target_path):
        if name in ("index.html", "_static"):
            # That's us, silly!
            continue
        path = os.path.join(target_path, name)
        relpath = os.path.relpath(path, target_path)
        base = os.path.basename(path)
        index_path = None
        soup = None
        index_path = None
        title = None
        image = None
        if os.path.isfile(path):
            (name, ext) = os.path.splitext(base)
            ext = ext.strip(".")
            is_file = True
            title = unslugify(name)
        elif os.path.isdir(path):
            (name, ext) = (os.path.basename(path), None)
            is_file = False
            if contains_index(path):
                index_path = get_index_path(path)
                soup = get_soup(index_path)
            else:
                log.debug("%s does not contain index.html, skpping...", path)
                continue
        if soup:
            logo = find_logo(soup)
            banner = find_banner(soup)
            favicon = find_favicon(soup)
            image = logo or banner or favicon
            # Ensure the image is relative to doc directory
            if image and os.path.isdir(path):
                image = os.path.join(relpath, image)
            title = find_title(soup)
        link = {
            "name": name,
            "path": relpath,
            "type": ext,
            "title": title,
            "image": image,
        }
        doc_links.append(link)
    return doc_links


def update_downdoc_index(cfg):
    tpl = dget(cfg, "template/path", default=None) or os.path.join(
        PROJ_ROOT, "templates/index.html"
    )
    static_dir = dget(cfg, "template/static_dir", default=None) or DEFAULT_STATIC_DIR
    target_path = get_target_path(cfg)
    out_html = os.path.join(target_path, "index.html")
    static_target = os.path.join(target_path, "_static")
    log.debug(
        "Copied %s to %s",
        static_dir,
        shutil.copytree(static_dir, static_target, dirs_exist_ok=True),
    )
    ctx = {
        "doc_links": collect_doc_links(cfg),
    }
    with open(tpl, "r") as TPL:
        with open(out_html, "w+") as HTML:
            HTML.write(Template(TPL.read()).render(ctx))
