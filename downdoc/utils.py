#!/usr/bin/python3
import os
import re
import shutil
from yaml import load, dump
import logging

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

log = logging.getLogger(__name__)


def _safe_open(path, mode, fail_silently):
    if not os.path.exists(path):
        if not fail_silently:
            raise Exception(f"'{path}' does not exist")
        return None
    try:
        return open(path, mode)
    except Error as e:
        if fail_silently:
            return None
        raise e


def easy_read(path, mode="r", fail_silently=True):
    f = _safe_open(path, mode, fail_silently)
    if not f:
        return None
    content = f.read()
    f.close()
    return content


def easy_read_first(*paths, **kwargs):
    """ Read a list of files, returning the content of the first one that exists.

    :param paths: List of paths to try

    :param mode: Mode of opening the file.

    :param fail_silently: Whether to raise an error if something happens.

    :return: The contents of the first file that reads successfully, or None
    """
    mode = kwargs.pop("mode", "r")
    fail_silently = kwargs.pop("fail_silently", True)
    for p in paths:
        content = easy_read(p, mode, fail_silently)
        if content:
            return content


def yaml_read_first(*paths, **kwargs):
    """ Read a list of files, returning the content of the first one that exists.

    :param paths: List of paths to try

    :param mode: Mode of opening the file.

    :param fail_silently: Whether to raise an error if something happens.

    :return: The contents of the first file that reads successfully, or None
    """
    mode = kwargs.pop("mode", "r")
    fail_silently = kwargs.pop("fail_silently", True)
    for p in paths:
        content = None
        f = _safe_open(p, mode=mode, fail_silently=fail_silently)
        if not f:
            continue
        content = load(f, Loader=Loader)
        if content:
            f.close()
            return content


def squash_path(path, base):
    """Find the first path with more than 1 file in it.

    Refactor that file to `base`
    """

    tmp_base = base + "__tmp"
    os.makedirs(tmp_base)
    moved = False
    for root, dirs, files in os.walk(path):
        if len(files + dirs) <= 1:
            continue
        for f in dirs + files:
            shutil.move(os.path.join(root, f), tmp_base)
            log.debug("Moved %s to %s", f, tmp_base)
            moved = True
        break
    if moved:
        shutil.rmtree(base)  # remove the original base
        os.rename(tmp_base, base)


def config_load_one(*paths, **kwargs):
    document = easy_read_first(*paths, fail_silently=kwargs.pop("fail_silently", True))
    return load(document, Loader=Loader)



def unslugify(text):
    return re.compile(r"[\-_\.]").sub(" ", text).title()